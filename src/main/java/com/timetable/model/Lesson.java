package com.timetable.model;

public class Lesson {
    private int id;
    private String lessonName, location, beginsAt, endsAt, day;

    public Lesson() {
    }

    public Lesson(String lessonName, String location, String beginsAt, String endsAt, String day) {
        this.lessonName = lessonName;
        this.location = location;
        this.beginsAt = beginsAt;
        this.endsAt = endsAt;
        this.day = day;
    }

    public Lesson(int id, String lessonName, String location, String beginsAt, String endsAt, String day) {
        this.id = id;
        this.lessonName = lessonName;
        this.location = location;
        this.beginsAt = beginsAt;
        this.endsAt = endsAt;
        this.day = day;
    }

    public int getId() {
        return id;
    }

    public String getLessonName() {
        return lessonName;
    }

    public String getLocation() {
        return location;
    }

    public String getBeginsAt() {
        return beginsAt;
    }

    public String getEndsAt() {
        return endsAt;
    }

    public String getDay() {
        return day;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setBeginsAt(String beginsAt) {
        this.beginsAt = beginsAt;
    }

    public void setEndsAt(String endsAt) {
        this.endsAt = endsAt;
    }

    public void setDay (String day) {
        this.day = day;
    }
}
