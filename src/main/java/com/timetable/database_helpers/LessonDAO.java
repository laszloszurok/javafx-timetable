package com.timetable.database_helpers;

import com.timetable.model.Lesson;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;

public class LessonDAO {
    private static final String CONNECTION = "jdbc:sqlite:database/timetable.db";
    private static final String ADD_LESSON = "insert into lesson (lesson_name, location, begins_at, ends_at, day) values (?, ?, ?, ?, ?)";
    private static final String GET_LESSONS = "select * from lesson";

    public int addLesson(Lesson lesson) throws SQLException {
        try (Connection connection = DriverManager.getConnection(CONNECTION);
             PreparedStatement pst = connection.prepareStatement(ADD_LESSON, Statement.RETURN_GENERATED_KEYS);) {

            pst.setString(1, lesson.getLessonName());
            pst.setString(2, lesson.getLocation());
            pst.setString(3, lesson.getBeginsAt());
            pst.setString(4, lesson.getEndsAt());
            pst.setString(5, lesson.getDay());

            int affectedRows = pst.executeUpdate();

            if (affectedRows != 1) {
                System.out.println("Error while executing insert");
            }

            ResultSet rs = pst.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);
            }
        }
        return -1;
    }

    public ObservableList<Lesson> getLessons() {
        ObservableList<Lesson> result = FXCollections.observableArrayList();
        try (
                Connection connection = DriverManager.getConnection(CONNECTION);
                Statement st = connection.createStatement();
        ) {
            ResultSet rs = st.executeQuery(GET_LESSONS);
            while (rs.next()) {
                Lesson lesson = new Lesson(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6)
                );
                result.add(lesson);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
