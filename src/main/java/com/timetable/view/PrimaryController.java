package com.timetable.view;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.timetable.database_helpers.LessonDAO;
import com.timetable.model.Lesson;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.kordamp.ikonli.javafx.FontIcon;

public class PrimaryController implements Initializable {
    private LessonDAO lessonDAO = new LessonDAO();

    private List<Lesson> mondayList = new ArrayList<>();
    private List<Lesson> tuesdayList = new ArrayList<>();
    private List<Lesson> wednesdayList = new ArrayList<>();
    private List<Lesson> thursdayList = new ArrayList<>();
    private List<Lesson> fridayList = new ArrayList<>();

    private String selectedDay = "monday";

    @FXML private VBox lessonListVBox;

    @FXML private Button addLessonButton;
    @FXML private Button settingsButton;

    @FXML private Button mondayButton;
    @FXML private Button tuesdayButton;
    @FXML private Button wednesdayButton;
    @FXML private Button thursdayButton;
    @FXML private Button fridayButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        getLessonsFromDB();
        displayLessons(selectedDay);

        // Switching to addNewLesson window
        addLessonButton.setOnAction(event -> {
            switchWindow(event, selectedDay, "addNewLesson");
        });

        // Switching to settings window
        settingsButton.setOnAction(event -> {
            switchWindow(event, selectedDay, "settings");
        });

        mondayButton.setOnAction(event -> {
            selectDay(mondayButton.getText().toLowerCase());
        });
        tuesdayButton.setOnAction(event -> {
            selectDay(tuesdayButton.getText().toLowerCase());
        });
        wednesdayButton.setOnAction(event -> {
            selectDay(wednesdayButton.getText().toLowerCase());
        });
        thursdayButton.setOnAction(event -> {
            selectDay(thursdayButton.getText().toLowerCase());
        });
        fridayButton.setOnAction(event -> {
            selectDay(fridayButton.getText().toLowerCase());
        });
    }

    @FXML
    private void closeApp() {
        Platform.exit();
    }

    public void selectDay(String day) {
        selectedDay = day;
        lessonListVBox.getChildren().clear();
        displayLessons(day);
    }

    /**
     * This method is used to switch scenes and pass the currently selected day to the scene
     * we are switching to, so we can return to the primary scene with the previously passed
     * day open later.
     * @param event
     * @param day
     * @param fxml
     */
    private void switchWindow(ActionEvent event, String day, String fxml) {
        Stage appStage;
        Parent root;
        try {
            if (event.getSource() == addLessonButton) {
                appStage = (Stage) addLessonButton.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml + ".fxml"));
                root = loader.load();

                AddNewLessonController addNewLessonController = loader.getController();
                addNewLessonController.passDay(day);

                Scene scene = new Scene(root);
                appStage.setScene(scene);
                appStage.show();
            } else if (event.getSource() == settingsButton) {
                appStage = (Stage) settingsButton.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml + ".fxml"));
                root = loader.load();

                SettingsController settingsController = loader.getController();
                settingsController.passDay(day);

                Scene scene = new Scene(root);
                appStage.setScene(scene);
                appStage.show();
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
            System.err.println(fxml + ".fxml not found");
        }
    }

    private void getLessonsFromDB() {
        ObservableList<Lesson> allLessons = lessonDAO.getLessons();
        allLessons.forEach(lesson -> {
            switch (lesson.getDay()) {
                case "monday":
                    mondayList.add(lesson);
                    break;
                case "tuesday":
                    tuesdayList.add(lesson);
                    break;
                case "wednesday":
                    wednesdayList.add(lesson);
                    break;
                case "thursday":
                    thursdayList.add(lesson);
                    break;
                case "friday":
                    fridayList.add(lesson);
                    break;
                default:
                    System.out.println("Unknown day");
            }
        });
    }

    private void displayLessons(String selectedDay) {
        switch (selectedDay) {
            case "monday":
                mondayList.forEach(lesson -> {
                    displayLessonCard(lesson.getLessonName(), lesson.getLocation(), lesson.getBeginsAt(), lesson.getEndsAt());
                });
                break;
            case "tuesday":
                tuesdayList.forEach(lesson -> {
                    displayLessonCard(lesson.getLessonName(), lesson.getLocation(), lesson.getBeginsAt(), lesson.getEndsAt());
                });
                break;
            case "wednesday":
                wednesdayList.forEach(lesson -> {
                    displayLessonCard(lesson.getLessonName(), lesson.getLocation(), lesson.getBeginsAt(), lesson.getEndsAt());
                });
                break;
            case "thursday":
                thursdayList.forEach(lesson -> {
                    displayLessonCard(lesson.getLessonName(), lesson.getLocation(), lesson.getBeginsAt(), lesson.getEndsAt());
                });
                break;
            case "friday":
                fridayList.forEach(lesson -> {
                    displayLessonCard(lesson.getLessonName(), lesson.getLocation(), lesson.getBeginsAt(), lesson.getEndsAt());
                });
                break;
            default:
                System.err.println("Unknown day");
        }
    }

    private void displayLessonCard(String lessonName, String location, String beginsAt, String endsAt) {
        HBox newLessonCard = constructLessonCard(lessonName, location, beginsAt, endsAt);
        lessonListVBox.getChildren().add(newLessonCard);
    }

    private HBox constructLessonCard(String lessonName, String location, String beginsAt, String endsAt) {
        HBox lessonCardHBox, lessonDataHBox, lessonActionsHBox;
        lessonCardHBox = new HBox();
        lessonDataHBox = new HBox();
        lessonActionsHBox = new HBox();

        VBox lessonDataVBox = new VBox();

        FontIcon lessonIcon = new FontIcon();

        FontIcon deleteIcon = new FontIcon("fa-trash");
        Button deleteLessonButton = new Button();
        deleteLessonButton.setGraphic(deleteIcon);

        FontIcon editIcon = new FontIcon("fa-edit");
        Button editLessonButton = new Button();
        editLessonButton.setGraphic(editIcon);

        Text lessonNameText, locationText, durationText;
        lessonNameText = new Text();
        locationText = new Text();
        durationText = new Text();

        lessonNameText.setText(lessonName);
        locationText.setText(location);
        durationText.setText(beginsAt + " - " + endsAt);

        lessonDataVBox.getChildren().addAll(lessonNameText, locationText, durationText);
        lessonDataHBox.getChildren().addAll(lessonIcon, lessonDataVBox);
        lessonActionsHBox.getChildren().addAll(editLessonButton, deleteLessonButton);
        lessonCardHBox.getChildren().addAll(lessonDataHBox, lessonActionsHBox);

        styleLessonCard(lessonCardHBox, lessonDataHBox, lessonActionsHBox, lessonNameText, lessonIcon, editIcon, deleteIcon, editLessonButton, deleteLessonButton);

        return lessonCardHBox;
    }

    private void styleLessonCard(HBox lessonCardHBox, HBox lessonDataHBox, HBox lessonActionsHBox, Text lessonNameText, FontIcon lessonIcon, FontIcon editIcon, FontIcon deleteIcon, Button editLessonButton, Button deleteLessonButton) {
        lessonCardHBox.setHgrow(lessonDataHBox, Priority.ALWAYS);
        lessonCardHBox.setStyle("-fx-background-color: #c2c2c2; -fx-background-radius: 5");

        lessonDataHBox.setAlignment(Pos.CENTER_LEFT);
        lessonDataHBox.setPadding(new Insets(10, 10, 10, 15));
        lessonDataHBox.setSpacing(20);

        lessonNameText.setFont(new Font("System", 16));

        lessonIcon.setIconLiteral("fa-clock-o");
        lessonIcon.setIconSize(8);
        lessonIcon.setIconColor(Paint.valueOf("#333"));
        lessonIcon.setScaleX(2);
        lessonIcon.setScaleY(2);

        lessonActionsHBox.setAlignment(Pos.CENTER_RIGHT);
        lessonActionsHBox.setPadding(new Insets(5, 5, 5, 5));
        lessonActionsHBox.setSpacing(5);

        editLessonButton.setStyle("-fx-background-color: none;");
        deleteLessonButton.setStyle("-fx-background-color: none;");
        editIcon.setScaleX(2);
        editIcon.setScaleY(2);
        deleteIcon.setScaleX(2);
        deleteIcon.setScaleY(2);
        editIcon.setIconColor(Paint.valueOf("#333"));
        deleteIcon.setIconColor(Paint.valueOf("#333"));
    }
}
