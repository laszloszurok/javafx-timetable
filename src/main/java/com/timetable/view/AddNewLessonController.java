package com.timetable.view;

import com.timetable.database_helpers.LessonDAO;
import com.timetable.model.Lesson;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class AddNewLessonController implements Initializable {
    private LessonDAO lessonDAO = new LessonDAO();
    private String selectedDay;

    @FXML private TextField lessonNameTextField;
    @FXML private TextField locationTextField;
    @FXML private TextField beginsAtTextField;
    @FXML private TextField endsAtTextField;

    @FXML private Button backButton;
    @FXML private Button cancelButton;
    @FXML private Button saveButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        backButton.setOnAction(event -> {
            switchToPrimary(event, selectedDay);
        });
        cancelButton.setOnAction(event -> {
            switchToPrimary(event, selectedDay);
        });

       saveButton.setOnAction(event -> {
           addLesson(event);
       });
    }

    @FXML private void switchToPrimary(ActionEvent event, String selectedDay) {
        Stage appStage;
        Parent root;
        try {
            if(event.getSource() == backButton || event.getSource() == cancelButton || event.getSource() == saveButton) {
                appStage = (Stage)backButton.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("primary.fxml"));
                root = loader.load();

                PrimaryController primaryController = loader.getController();
                primaryController.selectDay(selectedDay);

                Scene scene = new Scene(root);
                appStage.setScene(scene);
                appStage.show();
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
            System.err.println("Fxml file not found");
        }
    }

    @FXML private void closeApp() {
        Platform.exit();
    }

    @FXML private void addLesson(ActionEvent event) {
        Lesson lesson = createLessonObject(lessonNameTextField.getText(), locationTextField.getText(), beginsAtTextField.getText(), endsAtTextField.getText(), selectedDay);
        if (lesson != null) {
            try {
                lessonDAO.addLesson(lesson);
                lessonAdded(event);
            } catch (SQLException sqlException) {
                sqlException.printStackTrace();
            }
        }
    }

    private Lesson createLessonObject(String lessonName, String location, String beginsAt, String endsAt, String day) {
        if (lessonName == null || location == null || beginsAt == null || endsAt == null || day == null || lessonName.isEmpty() || location.isEmpty() || beginsAt.isEmpty() || endsAt.isEmpty() || day.isEmpty()) {
            return null;
        }
        return new Lesson(lessonName, location, beginsAt, endsAt, day);
    }

    public void passDay(String day) {
        selectedDay = day;
    }

    private void lessonAdded(ActionEvent event) {
        Dialog dialog = new Dialog();

        dialog.setTitle("Success");
        dialog.setHeaderText("Lesson saved");

        dialog.initStyle(StageStyle.UNDECORATED);
        dialog.getDialogPane().setPrefWidth(200);
        dialog.getDialogPane().setPrefHeight(50);

        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);

        dialog.showAndWait();
        switchToPrimary(event, selectedDay);
    }
}
