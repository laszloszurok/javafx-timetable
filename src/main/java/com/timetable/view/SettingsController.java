package com.timetable.view;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SettingsController implements Initializable {
    @FXML private Button backButton;
    private String selectedDay;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        backButton.setOnAction(event -> {
            switchToPrimary(event, selectedDay);
        });
    }

    @FXML private void switchToPrimary(ActionEvent event, String selectedDay) {
        Stage appStage;
        Parent root;
        try {
            if(event.getSource() == backButton) {
                appStage = (Stage)backButton.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("primary.fxml"));
                root = loader.load();

                PrimaryController primaryController = loader.getController();
                primaryController.selectDay(selectedDay);

                Scene scene = new Scene(root);
                appStage.setScene(scene);
                appStage.show();
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
            System.err.println("Fxml file not found");
        }
    }

    public void passDay(String day) {
        selectedDay = day;
    }

    @FXML private void closeApp() {
        Platform.exit();
    }
}
